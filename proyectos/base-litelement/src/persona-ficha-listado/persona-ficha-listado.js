import {LitElement,html,css} from 'lit-element';

class PersonaFichaListado extends LitElement{
    static get properties(){
          return {
           name:{type:String}, 
           yearsInCompany:{type:Number},
           photo:{type:Object},
           profile:{type:String} 
          };
    }

    constructor(){
        super();
    }
    
    render() {
        return html `
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
            <div class="card h-100">
                <img src="${this.photo.src}" alt="${this.photo.alt}" height="50" width="50" class="card-img-top"/>
                
                <div class="card-body">
                   <h5 class="card-title">${this.name}</h5>
                   <p class="card-text">${this.profile}</p>
                </div>
                
                <ul class="list-group list-group-flush">
                   <li class="list-group-item">
                      ${this.yearsInCompany} años en la empresa
                   </li>
                </ul> 

            </div> 
        `;
    }
}

customElements.define('persona-ficha-listado',PersonaFichaListado)