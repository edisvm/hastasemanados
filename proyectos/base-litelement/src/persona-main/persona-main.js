import {LitElement,html,css} from 'lit-element';
import '../persona-ficha-listado/persona-ficha-listado.js';

class PersonaMain extends LitElement{
    static get styles(){
        return css `
               :host{
                   all:initial;
               }
        `;
    }

    static get properties(){
          return {
                people:{type: Array}
          };
    }
    constructor(){
        super();
        this.people = [
             
             {
              "profile": "Lorem ipsum dolor sit amet.",
              "name": "Selena Mora",
              "yearsInCompany":10,
              "photo": {
                        "src": "./img/persona.jpg",
                        "alt": "Selena Mora"   
                      }
             },
             {
              "profile": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsuzm has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of", 
              "name": "Dolores Fuertes",
              "yearsInCompany":2,
              "photo": {
                        "src": "./img/persona.jpg",
                        "alt": "Dolores Fuertes"   
                       }          
             },
             {
              "profile": "Lorem", 
              "name": "Elena Nito del Bosque",
              "yearsInCompany":5,
              "photo": {
                        "src": "./img/persona.jpg",
                        "alt": "Elena Nito"   
                       }
             },
             {
              "profile": "Lorem ipsum dolor sit amet., consectur ...",
              "name": "Leandro Gao",
              "yearsInCompany":9,
              "photo": {
                       "src": "./img/persona.jpg",
                       "alt": "ELEandro Gao"   
                      }
             },
             {
              "profile": "Lorem ipsum sit amet",  
              "name": "Edison H Villaverde",
              "yearsInCompany":3,
              "photo": {
                       "src": "./img/persona.jpg",
                       "alt": "Edison"   
                     }
             }
        ];
    }

    render() {
        return html `
                <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
            <h2 class="text-center">Personas?</h2> 
            
            <main>
                <div class="row" id="peopleList">
                    <div class="row row-cols-1 row-cols-sm-4">
                        ${this.people.map(
                        person => html`<persona-ficha-listado name="${person.name}" yearsInCompany="${person.yearsInCompany}" profile="${person.profile}" .photo="${person.photo}"></persona-ficha-listado>`
                        )}
                    </div>
                </div>
            </main>
        `;
    }
}

customElements.define('persona-main',PersonaMain)